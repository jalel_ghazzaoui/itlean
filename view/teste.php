<?php
/**
 * Created by PhpStorm.
 * User: jalel
 * Date: 23/06/2016
 * Time: 15:21
 */
if($_POST[gravar]) {

    require_once("../model/model.php");

    $id = intval($_POST[id]);
    $descricao = addslashes($_POST[descricao]);

    $array[] = array('id' => $id, 'descricao' => $descricao);
    $jsonRequest = json_encode($array);

    $classe = new Itlean();
    $response = $classe->postItlean($jsonRequest);

    if ($response == true){ ?>
        <strong>Dados gravados com sucesso.</strong>
    <?php }
    else{ ?>
        <strong>Ops... Erro !</strong>
    <?php
    }

}

?>

<html>
    <body>
        <div>
            <form name="formCad" id="formCad" method="POST" action="teste.php">
                <label>Id:</label>
                <input type="text" name="id" id="id" size="2" maxlength="2" digits="true" autofocus>
                <label>Descri��o:</label>
                <input type="text" name="descricao" id="descricao" size="20" maxlength="20">
                <input type="submit" name="gravar" id="gravar" value="gravar">
                <input type="submit" name="listar" id="listar" value="listar">
            </form>
        </div>
    </body>
</html>


<?php

if($_POST[listar]) {

    require_once("../model/model.php");

    $classe = new Itlean();
    $jsonResponse = $classe->getItlean();
    if ($jsonResponse == false){ ?>
        <strong>Ops... Erro !</strong>
    <?php
    }
    else{
        $array = json_decode($jsonResponse,true);
        if (sizeof($array) > 0){ ?>
            <table border="1" cellspacing="0" width="400px">
            <tr>
                <th>ID</th>
                <th>DESCRICAO</th>
            </tr>
            <?php foreach($array as $key => $value){ ?>
                <tr>
                    <td><?php echo $value[id]; ?></td>
                    <td><?php echo $value[descricao]; ?></td>
                </tr>
            <?php } ?>
            </table>
        <?php
        }
        else{ ?>
            <strong>Ops... N�o temos nenhum registro para exibir.</strong>
        <?php }

    }

}

?>