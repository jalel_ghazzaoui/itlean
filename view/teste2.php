<script language="javascript" type="text/javascript">
    // cria a conex�o com o ajax
    function criaConexao(){
        var request = null;
        try{
            request = new XMLHttpRequest();
        }
        catch (trymicrosoft){
            try{
                request = new ActiveXObject("Msxml2.XMLHTTP");
            }
            catch (othermicrosoft){
                try{
                    request = new ActiveXObject("Microsoft.XMLHTTP");
                }
                catch (failed){
                    request = null;
                }
            }
        }
        if (request == null){
            alert ("Erro ao criar objeto de requisi��o");
        }
        return request;
    }

    function postDados(){
        var id = document.getElementById("id").value;
        var descricao = document.getElementById("descricao").value;

        var request = criaConexao();
        processarRequest(request,"post",id,descricao);
    }

    function processarRequest(request,tipo,id,descricao){

        var url = "../model/model2.php?tipo="+tipo+"&id="+id+"&descricao="+descricao;

        request.open("GET", url, true);
        request.send(null);
        request.onreadystatechange = function(){
            if (request.readyState == 4) {
                if (request.status == 200) {
                    if (tipo == 'post') {
                        var objeto = eval('(' + request.responseText + ')');
                        alert(objeto[0].mensagem);
                        document.getElementById("id").value = null;
                        document.getElementById("descricao").value = null;
                        getDados();
                    }
                    else {
                        alert ("Erro! O status da requisi��o � " + request.status);
                    }
                }
            }
        }
    }

    function getDados(){

        var request = criaConexao();

        var url = "../model/model2.php?tipo=get";

        var div = document.getElementById("dados");

        request.open("GET", url, true);
        request.send(null);
        request.onreadystatechange = function(){
            if (request.readyState == 4) {
                if (request.status == 200) {
                    //if (tipo == 'get') {
                        var objeto = eval('(' + request.responseText + ')');
                        //alert(objeto[0].mensagem);
                        table = "<table border='1' width='500px'>";
                        if (objeto.length == 0){
                            alert("Nenhum dado para ser exibido.");
                        }
                        else {
                            for (x = 0; x < objeto.length; x++) {
                                var id = objeto[x].id;
                                var descricao = objeto[x].descricao;
                                table += "<tr><td>"+id+"</td><td>"+descricao+"</td></tr>";
                            }
                        }
                        div.innerHTML = table;

                    //}
                    //else {
                    //    alert ("Erro! O status da requisi��o � " + request.status);
                    //}
                }
            }
        }

    }


</script>

<html>
    <body>
        <div>
            <form name="formCad" id="formCad" method="POST" action="teste2.php">
                <label>Id:</label>
                <input type="text" name="id" id="id" size="2" maxlength="2" digits="true" autofocus>
                <label>Descri��o:</label>
                <input type="text" name="descricao" id="descricao" size="20" maxlength="20">
                <input type="button" name="gravar" id="gravar" value="gravar" onclick="postDados();">
                <input type="button" name="listar" id="listar" value="listar" onclick="getDados();">
            </form>
        </div>
        <div id="dados" name="dados"></div>
    </body>
</html>